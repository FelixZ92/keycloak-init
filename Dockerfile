FROM registry.gitlab.com/felixz92/kcadm:master

ENV KEYCLOAK_DEFINITIONS_DIR /definitions
ENV KEYCLOAK_USER_SECRET_DIR /secrets

COPY *.sh ./

ENTRYPOINT ["/keycloak/init.sh"]
