#!/usr/bin/env sh

# check if keycloak is reachable
echo "operation=waiting"
until $(curl --output /dev/null --silent --head --fail "${KEYCLOAK_ENDPOINT}"); do
  printf '.'
  sleep 3
done

# authenticate
kcadm config credentials --server ${KEYCLOAK_ENDPOINT} --realm ${KEYCLOAK_REALM} --user ${KEYCLOAK_USER} --password ${KEYCLOAK_PASS}

if test ${?} -ne 0; then
 echo "operation=authentication error=true"
 exit 1
fi

for dir in "${KEYCLOAK_DEFINITIONS_DIR}"/realms/*; do
  realm=$(basename "${dir}")
  if test -f "${dir}/realm/definition.json"; then
    response=$(kcadm get "realms/${realm}")
    if test -z "${response}"; then
      echo "operation=create resource=realm name=${realm}"
      kcadm create realms -f "${dir}/realm/definition.json"
    else
      echo "operation=update resource=realm name=${realm}"
      kcadm update "realms/${realm}" -f "${dir}/realm/definition.json"
    fi
  fi
  if test -d "${dir}/client-scopes"; then
    for file in ${dir}/client-scopes/*; do
      scope=$(cat "${file}" | jq -r .name)
      scope_id=$(./kcadm.sh get client-scopes -r "${realm}" \
      | jq -r --arg client_scope_name "${scope}" '.[] | select(.name==$client_scope_name).id')
      if test -z "${scope_id}"; then
        echo "operation=create resource=client-scope name=${scope}"
        kcadm create client-scopes -r "${realm}" -f "${file}"
      else
        echo "operation=update resource=client-scope name=${scope}"
        kcadm update client-scopes/"${scope_id}" -r "${realm}" -f "${file}"
      fi
    done
  fi
  if test -d "${dir}/clients"; then
    for file in ${dir}/clients/*; do
      client=$(cat "${file}/definition/definition.json" | jq -r .clientId)
      secret=$(cat "${file}/secret/secret")
      client_id=$(./kcadm.sh get clients -r "${realm}" \
        | jq -r --arg client_id "${client}" '.[] | select(.clientId==$client_id).id')
      if test -z "${client_id}"; then
        echo "operation=create resource=client name=${client}"
        kcadm create clients -r "${realm}" -f "${file}/definition/definition.json" -s secret="${secret}"
        client_id=$(./kcadm.sh get clients -r "${realm}" \
          | jq -r --arg client_id "${client}" '.[] | select(.clientId==$client_id).id')
      else
        echo "operation=update resource=client name=${client}"
        kcadm update clients/"${client_id}" -r "${realm}" -f "${file}/definition/definition.json" -s secret="${secret}"
      fi
      grep -v '^ *#' < "${file}/definition/roles" | while IFS= read -r role
      do
        if ! kcadm get-roles -r k8s --cid k8s --rolename "${role}"; then
          kcadm create "clients/${client_id}/roles" -r "${realm}" -s name="${role}"
        fi
      done

    done
  fi
  if test -d "${dir}/groups"; then
    for file in ${dir}/groups/*; do
      group=$(cat "${file}" | jq -r .name)
      group_id=$(./kcadm.sh get groups -r "${realm}" \
      | jq -r --arg group_name "${group}" '.[] | select(.name==$group_name).id')
      if test -z "${group_id}"; then
        echo "operation=create resource=group name=${group}"
        kcadm create groups -r "${realm}" -f "${file}"
      else
        echo "operation=update resource=group name=${group}"
        kcadm update groups/"${group_id}" -r "${realm}" -f "${file}"
      fi
    done
  fi
  if test -d "${dir}/users"; then
    for file in ${dir}/users/*; do
      user=$(cat "${file}/definition.json" | jq -r .username)
      user_id=$(./kcadm.sh get users -r "${realm}" \
      | jq -r --arg user_name "${user}" '.[] | select(.username==$user_name).id')
      if test -z "${user_id}"; then
        echo "operation=create resource=user name=${user}"
        kcadm create users -r "${realm}" -f "${file}/definition.json"
      else
        echo "operation=update resource=user name=${user}"
        kcadm update users/"${user_id}" -r "${realm}" -f "${file}/definition.json"
      fi

      grep -v '^ *#' < "${file}/groups" | while IFS= read -r group
      do
        echo "operation=update resource=user name=${name} group=${group}"
        user_id=$(./kcadm.sh get users -r "${realm}" \
        | jq -r --arg user_name "${user}" '.[] | select(.username==$user_name).id')
        group_id=$(./kcadm.sh get groups -r "${realm}" \
        | jq -r --arg group_name "${group}" '.[] | select(.name==$group_name).id')
        kcadm update "users/${user_id}/groups/${group_id}" -r "${realm}" \
         -s realm="${realm}" \
         -s userId="${user_id}" \
         -s groupId="${group_id}" -n
      done
      if test -f "${KEYCLOAK_USER_SECRET_DIR}/${realm}/users/${user}/password"; then
        kcadm set-password -r "${realm}" --username "${user}" --new-password $(cat "${KEYCLOAK_USER_SECRET_DIR}/${realm}/users/${user}/password")
      fi
    done
  fi
done

echo "All done."
