#!/usr/bin/env bash

KEYCLOAK_DEFINITIONS_DIR=/home/felixz/src/infrastructure/images/keycloak-init/test/keycloak-init/definitions

for dir in ${KEYCLOAK_DEFINITIONS_DIR}/realms/*; do
  realm=$(basename "${dir}")
  if test -f "${dir}/definition.json"; then
    echo "create or update realm ${realm}"
  fi

    if test -d "${dir}/users"; then
    for file in "${dir}"/users/*; do
      echo $file
      while IFS= read -r group
      do
        echo "Line: $group"
      done < <(grep -v '^ *#' < "${file}/groups")
    done
  fi
done
